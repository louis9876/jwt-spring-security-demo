package com.tanwb.repository;

import com.tanwb.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByOpenid(String openid);
}
