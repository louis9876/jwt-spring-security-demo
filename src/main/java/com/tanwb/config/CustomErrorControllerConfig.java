package com.tanwb.config;

import com.tanwb.controller.CustomErrorController;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义异常处理器的配置类
 * @author tabwubo
 */
@Configuration
public class CustomErrorControllerConfig {

    private ServerProperties serverProperties;

    public CustomErrorControllerConfig(ServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

    @Bean
    public ErrorAttributes errorAttributes(){
        return new DefaultErrorAttributes(this.serverProperties.getError().isIncludeException());
    }

    @Bean
    public ErrorController errorController(ErrorAttributes errorAttributes){
        return new CustomErrorController(errorAttributes, this.serverProperties.getError().isIncludeException());
    }
}
