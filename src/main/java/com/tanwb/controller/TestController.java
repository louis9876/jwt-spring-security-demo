package com.tanwb.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping
    @PreAuthorize("hasAuthority('user:test')")
    public String test(){
        return "test success";
    }

    @GetMapping("/authority")
    @PreAuthorize("hasAuthority('admin:test')")
    public String authority(){
        return "test authority success";
    }

}
