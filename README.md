# jwt-spring-security-demo

#### 介绍

这是一个使用Spring Security和Spring Boot 2的JSON Web Token (JWT)的简单演示

#### 必要依赖

1. jdk1.8
2. mysql 5.7

#### 安装教程

1. 导入数据表，导入项目的resource目录下的mysql.sql
2. 调整application.yml中的相关配置，如数据库的连接用户名密码，微信的appid、appsecret
3. 运行JwtSpringSecurityDemoApplication即可启动项目

#### 使用说明

1. 测试接口：localhost:8080/test，这个接口需要用户登录且拥有USER的角色才能访问
2. 登录接口：localhost:8080/login 

注：可导入resource目录下的jwt-spring-security-demo.postman_collection.json到Postman，修改参数就可以使用了

#### 登录时序图

![登录时序图](https://gitee.com/tanwubo/jwt-spring-security-demo/raw/master/src/main/resources/img/jwt-spring-security-demo%E7%99%BB%E5%BD%95%E6%97%B6%E5%BA%8F%E5%9B%BE.jpg "登录时序图")

#### 详细介绍请参考博客
[Spring Boot 2结合Spring security + JWT实现微信小程序登录](https://blog.csdn.net/qq_22606825/article/details/100047498)